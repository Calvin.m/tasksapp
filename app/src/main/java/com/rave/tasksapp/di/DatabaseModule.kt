package com.rave.tasksapp.di

import androidx.room.Room
import com.rave.tasksapp.TasksApplication
import com.rave.tasksapp.model.local.TasksDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {
    @Provides
    fun providesContactDatabase(): TasksDatabase {
        return Room.databaseBuilder(
            TasksApplication.applicationContext(),
            TasksDatabase::class.java,
            "contactsDatabase"
        ).build()
    }
}
