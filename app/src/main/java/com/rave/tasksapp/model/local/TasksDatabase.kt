package com.rave.tasksapp.model.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.rave.tasksapp.model.local.entity.TaskEntity

@Database(entities = [TaskEntity::class], version = 1)
abstract class TasksDatabase : RoomDatabase() {
    abstract fun tasksDao(): TasksDao
}
