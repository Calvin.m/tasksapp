package com.rave.tasksapp.model

import com.rave.tasksapp.model.local.TasksDatabase
import com.rave.tasksapp.model.local.entity.TaskEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Tasks repo.
 *
 * @property tasksDB
 * @constructor Create empty Tasks repo
 */
class TasksRepo @Inject constructor(
    val tasksDB: TasksDatabase
) {
    val tasksDao = tasksDB.tasksDao()

    /**
     * Get tasks.
     *
     * @return
     */
    suspend fun getTasks(): List<TaskEntity> = withContext(Dispatchers.IO) {
        tasksDao.getTasks()
    }

    /**
     * Add task.
     *
     * @param task
     */
    suspend fun addTask(task: TaskEntity) = withContext(Dispatchers.IO) {
        tasksDao.addTask(task)
    }

    /**
     * Delete task.
     *
     * @param task
     */
    suspend fun deleteTask(task: TaskEntity) = withContext(Dispatchers.IO) {
        tasksDao.deleteTask(task)
    }
}
