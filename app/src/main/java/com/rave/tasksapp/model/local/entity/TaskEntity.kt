package com.rave.tasksapp.model.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tasks_table")
data class TaskEntity(
    @PrimaryKey(autoGenerate = true) val id: Int?,
    val name: String,
    val description: String,
    val completed: Boolean
)
