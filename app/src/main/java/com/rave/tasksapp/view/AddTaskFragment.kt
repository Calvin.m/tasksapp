package com.rave.tasksapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.rave.tasksapp.model.local.entity.TaskEntity
import com.rave.tasksapp.viewmodel.TasksViewModel

/**
 * Add task fragment.
 *
 * @constructor Create empty Add task fragment
 */
class AddTaskFragment : Fragment() {
    private val tasksViewModel by activityViewModels<TasksViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by tasksViewModel.viewState.collectAsState()
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Text("Create new task", fontSize = 28.sp)
                    TextField(
                        placeholder = { Text("Task Name") },
                        value = state.addTaskName,
                        onValueChange = {
                            tasksViewModel.addTaskNameChange(it)
                        }
                    )
                    TextField(
                        placeholder = { Text("Task Description") },
                        value = state.addTaskDescription,
                        onValueChange = {
                            tasksViewModel.addTaskDescriptionChange(it)
                        }
                    )
                    Button(onClick = {
                        tasksViewModel.insertNewTask(
                            TaskEntity(
                                null,
                                state.addTaskName,
                                state.addTaskDescription,
                                completed = false
                            )
                        )
                        if (!state.isLoading) {
                            findNavController().navigateUp()
                        }
                    }) {
                        Text("Save New Task")
                    }
                }
            }
        }
    }
}
