package com.rave.tasksapp.view

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.sp
import com.rave.tasksapp.model.local.entity.TaskEntity
import com.rave.tasksapp.viewmodel.TasksViewModel

@Composable
fun CollapsedContactView(
    item: TaskEntity,
    tasksViewModel: TasksViewModel
) {
    Box(
        Modifier
            .fillMaxWidth()
            .drawBehind {
                val strokeWidth = 1 * density
                val y = size.height - strokeWidth / 2
                drawLine(
                    Color.LightGray,
                    Offset(0f, y),
                    Offset(size.width, y),
                    strokeWidth
                )
            }
    ) {
        Column() {
            Row() {
                Column(Modifier.weight(4f)) {
                    Text(text = item.name, fontSize = 28.sp)
                }
                Button(onClick = {
                    tasksViewModel.onCollapseExpandToggle(
                        taskId = item.id ?: 0
                    )
                    tasksViewModel.getTasks()
                }, Modifier.weight(2f)) {
                    Text("Expand")
                }
            }
        }
    }
}
