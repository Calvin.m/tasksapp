package com.rave.tasksapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.rave.tasksapp.R
import com.rave.tasksapp.viewmodel.TasksViewModel

/**
 * Tasks list fragment.
 *
 * @constructor Create empty Tasks list fragment
 */
class TasksListFragment : Fragment() {
    private val tasksViewModel by activityViewModels<TasksViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by tasksViewModel.viewState.collectAsState()
                val expandedState by tasksViewModel.expandedCardIdsList.collectAsState()
                tasksViewModel.getTasks()
                Column(Modifier.padding(8.dp)) {
                    Row( Modifier.drawBehind {
                        val strokeWidth = 1 * density
                        val y = size.height - strokeWidth / 2
                        drawLine(
                            Color.Black,
                            Offset(0f, y),
                            Offset(size.width, y),
                            strokeWidth
                        )
                    }.padding(8.dp)) {
                        Text(
                            "Tasks",
                            Modifier.weight(2f),
                            textAlign = TextAlign.Center,
                            fontSize = 24.sp
                        )
                        Button(
                            onClick = {
                                findNavController().navigate(R.id.addTaskFragment)
                            },
                            Modifier
                                .weight(.5f)
                                .padding(0.dp)
                        ) {
                            Text("+", fontSize = 26.sp)
                        }

                        if (state.isLoading) {
                            CircularProgressIndicator()
                        }
                    }
                    LazyColumn() {
                        items(state.tasksList) { item ->
                            if (expandedState.contains(item.id)) {
                                ExpandedContactView(item, tasksViewModel)
                            } else {
                                CollapsedContactView(item, tasksViewModel)
                            }
                        }
                    }
                }
            }
        }
    }
}
