package com.rave.tasksapp.viewmodel

import com.rave.tasksapp.model.local.entity.TaskEntity

/**
 * Tasks list state.
 *
 * @property tasksList
 * @property addTaskName
 * @property addTaskDescription
 * @property isLoading
 * @constructor Create empty Tasks list state
 */
data class TasksListState(
    val tasksList: List<TaskEntity> = emptyList(),
    val addTaskName: String = "",
    val addTaskDescription: String = "",
    val isLoading: Boolean = false
)
