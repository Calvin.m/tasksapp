package com.rave.tasksapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.tasksapp.model.TasksRepo
import com.rave.tasksapp.model.local.entity.TaskEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Tasks view model.
 *
 * @property repo
 * @constructor Create empty Tasks view model
 */
@HiltViewModel
class TasksViewModel @Inject constructor(
    private val repo: TasksRepo
) : ViewModel() {
    private val _viewState = MutableStateFlow(TasksListState())
    val viewState get() = _viewState.asStateFlow()

    private val _expandedCardIdsList = MutableStateFlow(listOf<Int>())
    val expandedCardIdsList: StateFlow<List<Int>> get() = _expandedCardIdsList

    init {
        getTasks()
    }

    /**
     * Get tasks.
     *
     */
    fun getTasks() {
        viewModelScope.launch {
            _viewState.update { it.copy(isLoading = true) }
            val tasksList = repo.getTasks()
            _viewState.update { it.copy(tasksList = tasksList, isLoading = false) }
        }
    }

    /**
     * Delete task.
     *
     * @param task
     */
    fun deleteTask(task: TaskEntity) {
        viewModelScope.launch {
            repo.deleteTask(task)
        }
    }

    /**
     * Insert new task.
     *
     * @param task
     */
    fun insertNewTask(task: TaskEntity) = viewModelScope.launch {
        _viewState.update { it.copy(isLoading = true) }
        repo.addTask(task)
        _viewState.update { it.copy(isLoading = false) }
    }

    /**
     * Add task name change.
     *
     * @param name
     */
    fun addTaskNameChange(name: String) {
        _viewState.update { it.copy(addTaskName = name) }
    }

    /**
     * Add task description change.
     *
     * @param descr
     */
    fun addTaskDescriptionChange(descr: String) {
        _viewState.update { it.copy(addTaskDescription = descr) }
    }

    /**
     * On collapse expand toggle.
     *
     * @param taskId
     */
    fun onCollapseExpandToggle(taskId: Int) {
        _expandedCardIdsList.value = _expandedCardIdsList.value.toMutableList().also { list ->
            if (list.contains(taskId)) list.remove(taskId) else list.add(taskId)
        }
    }
}
