package com.rave.tasksapp

import android.app.Application
import android.content.Context
import dagger.hilt.android.HiltAndroidApp

/**
 * Tasks application.
 *
 * @constructor Create empty Tasks application
 */
@HiltAndroidApp
class TasksApplication : Application() {
    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
    }

    companion object {
        private var instance: TasksApplication? = null

        /**
         * Application context.
         *
         * @return
         */
        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }
}
